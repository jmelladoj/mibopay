/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE IF NOT EXISTS `accesos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `creacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingreso_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accesos_ingreso_id_foreign` (`ingreso_id`),
  CONSTRAINT `accesos_ingreso_id_foreign` FOREIGN KEY (`ingreso_id`) REFERENCES `ingresos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=436 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `ingresos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip_ingreso` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ultimo_acceso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad_accesos` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `inscripcion_tarjetas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tbk_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_webpay` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo_autorizacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarjeta_tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarjeta_numero` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tbk_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inscripcion_tarjetas_user_id_foreign` (`user_id`),
  CONSTRAINT `inscripcion_tarjetas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `keys` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `creacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiracion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_ingreso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_mibo` bigint(20) DEFAULT NULL,
  `id_emp` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `usuario_pagos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `orden_compra` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarjeta_utilizada` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_transaccion` timestamp NULL DEFAULT NULL,
  `usuario_tarjeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_pagos_user_id_foreign` (`user_id`),
  KEY `usuario_pagos_usuario_tarjeta_id_foreign` (`usuario_tarjeta_id`),
  CONSTRAINT `usuario_pagos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `usuario_pagos_usuario_tarjeta_id_foreign` FOREIGN KEY (`usuario_tarjeta_id`) REFERENCES `usuario_tarjetas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `usuario_pago_detalles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monto` int(11) NOT NULL,
  `estado_webpay` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `codigo_autorizacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_tarjeta_codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` smallint(6) DEFAULT NULL,
  `codigo_comercio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden_compra_hijo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_pago_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_pago_detalles_usuario_pago_id_foreign` (`usuario_pago_id`),
  CONSTRAINT `usuario_pago_detalles_usuario_pago_id_foreign` FOREIGN KEY (`usuario_pago_id`) REFERENCES `usuario_pagos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `usuario_tarjetas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarjeta_tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarjeta_numero` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tbk_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `defecto` smallint(6) DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_tarjetas_user_id_foreign` (`user_id`),
  CONSTRAINT `usuario_tarjetas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
