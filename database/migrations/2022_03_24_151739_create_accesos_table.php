<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('creacion');

            $table->unsignedBigInteger('ingreso_id');
            $table->foreign('ingreso_id')->references('id')->on('ingresos');

            //$table->unsignedBigInteger('token_id');
            //$table->foreign('token_id')->references('id')->on('tokens');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesos');
    }
}
