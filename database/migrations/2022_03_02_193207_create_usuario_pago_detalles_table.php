<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioPagoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_pago_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('detalle');
            $table->integer('monto');
            $table->string('estado_webpay');
            $table->string('codigo_autorizacion')->nullable();
            $table->string('tipo_tarjeta_codigo')->nullable();
            $table->smallInteger('estado')->nullable();
            $table->string('codigo_comercio');
            $table->string('orden_compra_hijo');

            $table->unsignedBigInteger('usuario_pago_id');
            $table->foreign('usuario_pago_id')->references('id')->on('usuario_pagos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_pago_detalles');
    }
}
