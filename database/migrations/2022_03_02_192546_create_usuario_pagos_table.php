<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_pagos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('orden_compra');
            $table->string('tarjeta_utilizada');
            $table->string('fecha_transaccion');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('usuario_tarjeta_id');
            $table->foreign('usuario_tarjeta_id')->references('id')->on('usuario_tarjetas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_pagos');
    }
}
