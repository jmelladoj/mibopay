<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioTarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_tarjetas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('descripcion')->nullable();

            $table->string('tarjeta_tipo')->nullable();
            $table->string('tarjeta_numero')->nullable();
            $table->string('tbk_user')->nullable();
            $table->smallInteger('defecto')->default(0);

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_tarjetas');
    }
}
