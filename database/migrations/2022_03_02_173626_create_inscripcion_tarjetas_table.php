<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscripcionTarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion_tarjetas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('descripcion')->nullable();
            $table->text('tbk_token');
            $table->text('url_webpay');
            $table->string('codigo_autorizacion')->nullable();
            $table->string('tarjeta_tipo')->nullable();
            $table->string('tarjeta_numero')->nullable();
            $table->string('tbk_user')->nullable();
            $table->smallInteger('estado')->default(0);

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion_tarjetas');
    }
}
