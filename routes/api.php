<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/usuario/validar', 'UsuarioController@validar')->name('api.usuario.validar')->middleware('verificar.accesos');

Route::get('/usuario/pago/automatico/{usuario}/{monto}', 'UsuarioController@pagoAutomatico')->name('api.usuario.pago.automatico');
Route::get('/usuario/pago/estado/{numero_orden}', 'UsuarioController@pagoEstado')->name('api.usuario.pago.estado');
Route::post('/usuario/pagos', 'UsuarioController@pagosUsuario')->name('api.usuario.pagos');
Route::get('/token', 'TokenController@generar')->name('api.token.generar')->middleware('verificar.accesos');;

Route::group(['middleware' => ['verificar.accesos']], function () {
    Route::post('/usuario/pago/automatico', 'UsuarioController@postPagoAutomatico')->name('api.usuario.post.pago.automatico');
    Route::post('/usuario/pago/estado', 'UsuarioController@postPagoEstado')->name('api.usuario.post.pago.estado');
    Route::post('/usuario/eliminar/tarjeta', 'UsuarioController@postEliminarTarjeta')->name('api.usuario.post.pago.estado');
    Route::get('/usuario/listado/transacciones', 'UsuarioController@transaccionesListado')->name('api.usuario.transacciones.listado');

    Route::post('/usuario/eliminar/tarjeta', 'UsuarioController@eliminarTarjeta')->name('api.usuario.eliminar.tarjeta');
    Route::post('/usuario/cambiar/tarjeta/defecto', 'UsuarioController@cambiarTarjetaDefecto')->name('api.usuario.cambiar.tarjeta.defecto');
});

Route::group(['middleware' => ['cors']], function () {
    //Route::get('/usuario/listado/tarjetas/{id}', 'UsuarioController@tarjetasListado')->name('api.usuario.tarjetas.listado');
    Route::post('/usuario/listado/tarjetas', 'UsuarioController@usuarioListadoTarjetas')->name('api.usuario.listado.tarjetas');
    Route::post('/usuario/cantidad/tarjetas', 'UsuarioController@usuarioCantidadTarjetas')->name('api.usuario.cantidad.tarjetas');

    Route::post('/usuario/inscribir/tarjeta', 'UsuarioController@generarResponseWebapy')->name('api.usuario.inscribir.tarjeta');
    Route::post('/usuario/finalizar/inscribir/tarjeta', 'UsuarioController@finalizarInscripcionTajeta')->name('api.usuario.finalizar.inscribir.tarjeta');
    Route::get('/usuario/finalizar/inscribir/tarjeta', 'UsuarioController@finalizarInscripcionTajeta')->name('api.usuario.finalizar.inscribir.tarjeta');
    Route::put('/usuario/finalizar/inscribir/tarjeta', 'UsuarioController@finalizarInscripcionTajeta')->name('api.usuario.finalizar.inscribir.tarjeta');
    Route::post('/usuario/buscar/tarjeta', 'UsuarioController@buscarTarjeta')->name('api.usuario.busar.tarjeta');
    Route::post('/usuario/borrar/tarjeta', 'UsuarioController@borrarTarjeta')->name('api.usuario.borrar.tarjeta');
    Route::post('/usuario/cambiar/tarjeta/por/defecto', 'UsuarioController@cambiarTarjetaPorDefecto')->name('api.usuario.cambiar.tarjeta.por.defecto');
    Route::post('/usuario/realizar/pagos', 'UsuarioController@usuarioRealizarPagos')->name('api.usuario.realizar.pagos');
});
