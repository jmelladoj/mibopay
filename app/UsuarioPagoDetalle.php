<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UsuarioPagoDetalle extends Model
{
    //
    protected $guarded = ['id'];
    protected $append = ['fecha', 'estado_detalle'];

    public function getFechaAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y H:i:s');
    }

    public function getEstadoDetalleAttribute()
    {
        switch ($this->estado) {
            case -96:
                $mensaje = "Usuario no existente";
                break;

            case -97:
                $mensaje = "Máximo monto diario de pago excedido";
                break;
            case -98:
                $mensaje = "Máximo monto de pago excedido";
                break;
            case -99:
                $mensaje = "Máxima cantidad de pagos diarios excedido";
                break;
            default:
                $mensaje = "Aprobado";
                break;
        }

        return $mensaje;
    }

    public function pago()
    {
        return $this->belongsTo(UsuarioPago::class, 'usuario_pago_id');
    }
}
