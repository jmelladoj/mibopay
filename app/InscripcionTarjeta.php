<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscripcionTarjeta extends Model
{
    //
    protected $guarded = ['id'];
    protected $append = ['fecha', 'estado_detalle'];

    public function getEstadoDetalleAttribute()
    {
        switch ($this->estado) {
            case 0:
                $mensaje = "Tarjeta registrada correctamente, por favor vuielve a seleccionar tu pago.";
                break;

            case -1:
                $mensaje = "Posible error en el ingreso de datos de la transacción.";
                break;

            case -2:
                $mensaje = "Se produjo fallo al procesar la transacción, este mensaje de rechazo se encuentra relacionado a parámetros de la tarjeta y/o su cuenta asociada.";
                break;
            case -3:
                $mensaje = "Error en Transacción.";
                break;
            case -4:
                $mensaje = "Rechazada por parte del emisor.";
                break;
            case -5:
                $mensaje = "Transacción con riesgo de posible fraude.";
                break;
            default:
                $mensaje = "Problemas al registrar tarjeta.";
                break;
        }

        return $mensaje;
    }
}
