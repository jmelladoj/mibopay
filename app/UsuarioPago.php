<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UsuarioPago extends Model
{
    //
    protected $guarded = ['id'];
    protected $appends = ['total_pago', 'fecha', 'usuario_nombre', 'usuario_rut'];

    public function getUsuarioNombreAttribute()
    {
        return $this->usuario->nombre;
    }

    public function getUsuarioRutAttribute()
    {
        return $this->usuario->rut;
    }

    public function getFechaAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_transaccion)->format('d-m-Y H:i:s');
    }

    public function getTotalPagoAttribute()
    {
        return $this->detalle->sum('monto');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function detalle()
    {
        return $this->hasMany(UsuarioPagoDetalle::class, 'usuario_pago_id');
    }
}
