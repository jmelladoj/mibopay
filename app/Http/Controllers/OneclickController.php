<?php

namespace App\Http\Controllers;

use App\InscripcionTarjeta;
use App\UsuarioPago;
use App\UsuarioPagoDetalle;
use App\UsuarioTarjeta;
use Illuminate\Http\Request;
use Transbank\Webpay\Options;
use Transbank\Webpay\Oneclick\MallInscription;
use Transbank\Webpay\Oneclick\MallTransaction;
use Transbank\Webpay\Oneclick;

class OneclickController extends Controller
{

    public function __construct(){
        if (app()->environment('production')) {
            Oneclick::configureForProduction(config('services.transbank.oneclick_mall_cc'), config('services.transbank.oneclick_mall_api_key'));
        } else {
            Oneclick::configureForTesting();
        }
    }

    public function startInscription(Request $request)
    {

        session_start();

        $req = $request->except('_token');
        $userName = $req["user_name"];
        $email = $req["email"];
        //$responseUrl = $req["response_url"];
        $responseUrl = "http://127.0.0.1:8000/oneclick/responseUrl";

        $resp = (new MallInscription)->start($userName, $email, $responseUrl);

        $_SESSION["user_name"] = $userName;
        $_SESSION["email"] = $email;

        InscripcionTarjeta::create([
            'user_id' => 1,
            'tbk_token' => $resp->getToken(),
            'url_webpay' => $resp->getUrlWebpay()
        ]);

        return view('oneclick/inscription_successful', ['resp' => $resp, 'req' => $req]);
    }

    public function finishInscription(Request $request)
    {
        session_start();

        $req = $request->except('_token');
        $token = $req["TBK_TOKEN"];

        $resp = (new MallInscription)->finish($token);
        $userName = array_key_exists("user_name", $_SESSION) ? $_SESSION["user_name"] : '';

        $inscripcion = InscripcionTarjeta::where('tbk_token', $req['TBK_TOKEN'])->first();

        if($resp->responseCode == 0){
            $inscripcion->codigo_autorizacion = $resp->authorizationCode;
            $inscripcion->tarjeta_tipo = $resp->cardType;
            $inscripcion->tarjeta_numero = $resp->cardNumber;
            $inscripcion->tbk_user = $resp->tbkUser;
            $inscripcion->estado = 1;
            $inscripcion->save();

            UsuarioTarjeta::create([
                'tarjeta_tipo' => $resp->cardType,
                'tarjeta_numero' => $resp->cardNumber,
                'tbk_user' => $resp->tbkUser,
                'user_id' => $inscripcion->user_id
            ]);
        }

        return view('oneclick/inscription_finished', ["resp" => $resp, "req" => $req, "username" => $userName]);

    }

    public function authorizeMall(Request $request)
    {
        session_start();
        $req = $request->except('_token');

        $userName = $req["username"];
        $tbkUser = $req["tbk_user"];
        $parentBuyOrder = $req["buy_order"];
        $childBuyOrder = $req["details"][0]["buy_order"];
        $amount = $req["details"][0]["amount"];
        $installmentsNumber = $req["details"][0]["installments_number"];
        $childCommerceCode = $req["details"][0]["commerce_code"];

        $details = [
            [
                "commerce_code" => $childCommerceCode,
                "buy_order" => $childBuyOrder,
                "amount" => $amount,
                "installments_number" => $installmentsNumber
            ]
        ];


        $resp = (new MallTransaction)->authorize($userName, $tbkUser, $parentBuyOrder, $details);

        $usuarioTarjeta = UsuarioTarjeta::where('tbk_user', $tbkUser)->first();

        $pago = UsuarioPago::create([
            'cuotas' => $installmentsNumber,
            'codigo_comercio' => $childCommerceCode,
            'orden_compra' => $parentBuyOrder,
            'orden_compra_hijo' => $childBuyOrder,
            'user_id' => 1,
            'usuario_tarjeta_id' => $usuarioTarjeta->id,
        ]);

        UsuarioPagoDetalle::create([
            'detalle' => 'Detalle',
            'monto' => $amount,
            'usuario_pago_id' => $pago->id
        ]);

        return view('oneclick/authorized_mall', ["req" => $req, "resp" => $resp]);

    }

    public function transactionStatus(Request $request)
    {
        $req = $request->except('_token');
        $buyOrder = $req["buy_order"];

        $resp = (new MallTransaction)->status($buyOrder);

        $usuarioPago = UsuarioPago::where('orden_compra', $resp->buyOrder)->first();

        if($resp->details[0]->responseCode == 0){
            $usuarioPago->codigo_autorizacion = $resp->details[0]->authorizationCode;
            $usuarioPago->tipo_tarjeta_codigo = $resp->details[0]->paymentTypeCode;
            $usuarioPago->estado = 1;
        } else {
            $usuarioPago->estado = 2;
        }

        $usuarioPago->save();


        return view('oneclick/mall_transaction_status', ["req" => $req, "resp" => $resp]);
    }

    public function refund(Request $request)
    {
        $req = $request->except('_token');
        $buyOrder = $req["parent_buy_order"];
        $childCommerceCode = $req["commerce_code"];
        $childBuyOrder = $req["child_buy_order"];
        $amount = $req["amount"];

        $resp = (new MallTransaction)->refund($buyOrder, $childCommerceCode, $childBuyOrder, $amount);

        return view('oneclick/mall_refund_transaction', ["req" => $req, "resp" => $resp]);
    }

    public function deleteInscription(Request $request)
    {
        $req = $request->except('_token');
        $tbkUser = $req["tbk_user"];
        $userName = $req["user_name"];

        $inscripcion = InscripcionTarjeta::where('tbk_user', $request->tbk_user)->first();
        $inscripcion->estado = 3;
        $inscripcion->save();

        UsuarioTarjeta::where('tbk_user', $request->tbk_user)->delete();

        $resp = (new MallInscription)->delete($tbkUser, $userName);
        return view('oneclick/mall_inscription_deleted', ["req" => $req, "resp" => $resp]);
    }



}
