<?php

namespace App\Http\Controllers;

use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class TokenController extends Controller
{
    //
    public function generar(Request $request)
    {
        $token = base64_encode(Str::random(30));

        $token = Token::create([
            'token' => $token,
            'creacion' => Carbon::now()->format('Y-m-d H:i:s'),
            'expiracion' => Carbon::now()->addSeconds(10)->format('Y-m-d H:i:s'),
            'ip_ingreso' => $request->getClientIp()
        ]);

        return response()->json([
            'token' => $token
        ]);
    }
}
