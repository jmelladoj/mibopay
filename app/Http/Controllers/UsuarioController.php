<?php

namespace App\Http\Controllers;

use App\InscripcionTarjeta;
use App\User;
use App\UsuarioPago;
use App\UsuarioPagoDetalle;
use App\UsuarioTarjeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Transbank\Webpay\Oneclick\MallInscription;
use Transbank\Webpay\Oneclick\MallTransaction;
use Rut;
use DataTables;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

\Transbank\Webpay\Oneclick::configureForProduction(597046009447, '4c2da037-b561-4b82-b50a-b9824193596e');

class UsuarioController extends Controller
{
    //

    public function validar(Request $request)
    {
        $usuario = User::where('rut', base64_decode($request->rut))->first();

        $response = [
            'estado' => true,
            'errores' => []
        ];

        $validacionRut = Rut::parse(base64_decode($request->rut))->validate();
        $validacionRutRepresentante = Rut::parse(base64_decode($request->rutRepresentante))->validate();

        if (!isset($request->nombre) || !preg_match("/^[a-zA-Z\sñáéíóúÁÉÍÓÚ]+$/", base64_decode($request->nombre))) {
            array_push($response['errores'], 'Nombre no ingresado o formato incorrecto');
        }

        if (!isset($request->rut) || !$validacionRut) {
            array_push($response['errores'], 'Rut no ingresado o rut no valido');
        }

        if (!isset($request->rutRepresentante) || !$validacionRutRepresentante) {
            array_push($response['errores'], 'Rut de representante no ingresado o rut no valido');
        }

        if (!isset($request->email) || !preg_match("/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/", base64_decode($request->email))) {
            array_push($response['errores'], 'Email no ingresado o formato incorrecto');
        }

        if (!$usuario) {
            $usuario = User::create([
                'nombre' => base64_decode($request->nombre),
                'rut' => base64_decode($request->rut),
                'rut_representante' => base64_decode($request->rutRepresentante),
                'email' => base64_decode($request->email)
            ]);
        }

        if (count($response['errores']) > 0) {
            $response['estado'] = false;

            return $response;
        }

        //$responseUrl = env('RESPONSE_URL');
        //$resp = (new MallInscription)->start($usuario->nombre, $usuario->email, "http://52.21.229.183/");
        //$resp = (new MallInscription)->start($usuario->nombre, $usuario->email, $request->getSchemeAndHttpHost() . '/');
        //$resp = (new MallInscription)->start($usuario->nombre, $usuario->email, $responseUrl);

        //$resp = (new MallInscription)->start($usuario->nombre, $usuario->email, 'https://dhlqrot20d4v3.cloudfront.net/');

        $resp = (new MallInscription)->configureForProduction(597046009447, '4c2da037-b561-4b82-b50a-b9824193596e')->start($usuario->nombre, $usuario->email, 'https://dhlqrot20d4v3.cloudfront.net/');

        InscripcionTarjeta::create([
            'user_id' => $usuario->id,
            'tbk_token' => $resp->getToken(),
            'url_webpay' => $resp->getUrlWebpay(),
            'descripcion' => $request->descripcion
        ]);

        $response['tbk_token'] = $resp->getToken();
        $response['url_webpay'] = $resp->getUrlWebpay();

        return response()->json($response);
    }

    public function finalizar(Request $request)
    {
        $tarjetaRegistrada = 0;
        $inscripcion = InscripcionTarjeta::where('tbk_token', $request->TBK_TOKEN)->first();

        $resp = (new MallInscription)->finish($request->TBK_TOKEN);

        $inscripcion->codigo_autorizacion = $resp->authorizationCode;
        $inscripcion->tarjeta_tipo = $resp->cardType;
        $inscripcion->tarjeta_numero = $resp->cardNumber;
        $inscripcion->tbk_user = $resp->tbkUser;
        $inscripcion->estado = $resp->responseCode;
        $inscripcion->save();

        if ($resp->responseCode == 0) {
            $tarjeta = UsuarioTarjeta::where('tbk_user', $resp->tbkUser)->first();

            if (!$tarjeta) {
                $usuarioTarjeta = UsuarioTarjeta::create([
                    'tarjeta_tipo' => $resp->cardType,
                    'tarjeta_numero' => $resp->cardNumber,
                    'tbk_user' => $resp->tbkUser,
                    'user_id' => $inscripcion->user_id,
                    'descripcion' => $inscripcion->descripcion
                ]);
            } else {
                $tarjetaRegistrada = 1;
            }
        }

        return view('resultado', compact('inscripcion', 'tarjetaRegistrada'));
    }

    public function tarjetas($id)
    {
        return view('ktipay.tarjetas');
    }

    public function tarjetasListado($id)
    {
        return Datatables::of(UsuarioTarjeta::where('user_id', $id)->get())->make(true);
    }

    /* -------------------------------------------------------------------------------- */

    public function generarResponseWebapy(Request $request)
    {
        //$usuario =  base64_decode($request->usuario);

        $validator = Validator::make($request->all(), [
            'usuario' => 'required|json',
            'descripcion' => 'required|min:3',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true
            ]);
        }

        $usuario = json_decode($request->usuario, true);
        $descripcion = base64_decode($request->descripcion);

        $usuarioRegistrado = User::updateOrCreate([
            'id_mibo' => $usuario["id_usu"]
        ], [
            'nombre' => $usuario["nombres_usu"] . ' ' . $usuario["apellidos_usu"],
            'rut' => $usuario["rut_usu"] . '-' . $usuario["dv_usu"],
            'id_emp' => $usuario["id_emp"],
            'id_mibo' => $usuario["id_usu"],
            'email' => 'j.melladojimenez@hotmail.com',
            //'email' => $usuario["email"]
        ]);

        //$resp = (new MallInscription)->start($usuarioRegistrado->nombre, $usuarioRegistrado->email, "https://oneclick.test/api/usuario/finalizar/inscribir/tarjeta");
        //$resp = (new MallInscription)->start($usuarioRegistrado->nombre, $usuarioRegistrado->email, "http://52.21.229.183/api/usuario/finalizar/inscribir/tarjeta");
        //$resp = (new MallInscription)->start($usuarioRegistrado->nombre, $usuarioRegistrado->email, $request->getSchemeAndHttpHost() . "/api/usuario/finalizar/inscribir/tarjeta");

        $resp = (new MallInscription)->start($usuarioRegistrado->nombre, $usuarioRegistrado->email, "https://dhlqrot20d4v3.cloudfront.net/api/usuario/finalizar/inscribir/tarjeta");

        InscripcionTarjeta::create([
            'user_id' => $usuarioRegistrado->id,
            'tbk_token' => $resp->getToken(),
            'url_webpay' => $resp->getUrlWebpay(),
            'descripcion' => $request->descripcion
        ]);

        $response['tbk_token'] = $resp->getToken();
        $response['url_webpay'] = $resp->getUrlWebpay();

        return response()->json($response);
    }

    public function usuarioListadoTarjetas(Request $request)
    {
        $usuario = base64_decode($request->usuario);
        $usuarioTarjeta = User::where('id_mibo', $usuario)->first();

        return Datatables::of(UsuarioTarjeta::where('user_id', $usuarioTarjeta->id)->get())->make(true);
    }

    public function usuarioCantidadTarjetas(Request $request)
    {
        $usuario = base64_decode($request->usuario);
        $usuarioTarjeta = User::where('id_mibo', $usuario)->first();

        return response()->json(
            [
                'cantidad' => $usuarioTarjeta ? UsuarioTarjeta::where('user_id', $usuarioTarjeta->id)->count() : 0
            ]
        );
    }


    public function finalizarInscripcionTajeta(Request $request)
    {
        $tarjetaRegistrada = 0;
        $inscripcion = InscripcionTarjeta::where('tbk_token', $request->TBK_TOKEN)->first();

        //dd($inscripcion);


        $resp = (new MallInscription)->finish($request->TBK_TOKEN);

        $inscripcion->codigo_autorizacion = $resp->authorizationCode;
        $inscripcion->tarjeta_tipo = $resp->cardType;
        $inscripcion->tarjeta_numero = $resp->cardNumber;
        $inscripcion->tbk_user = $resp->tbkUser;
        $inscripcion->estado = $resp->responseCode;
        $inscripcion->save();

        if ($resp->responseCode == 0) {
            $tarjeta = UsuarioTarjeta::where('tbk_user', $resp->tbkUser)->first();

            if (!$tarjeta) {
                $usuarioTarjeta = UsuarioTarjeta::create([
                    'tarjeta_tipo' => $resp->cardType,
                    'tarjeta_numero' => $resp->cardNumber,
                    'tbk_user' => $resp->tbkUser,
                    'user_id' => $inscripcion->user_id,
                    'descripcion' => $inscripcion->descripcion
                ]);

                $tarjetas = UsuarioTarjeta::where('user_id', $inscripcion->user_id)->get()->count();

                $usuarioTarjeta->defecto = $tarjetas == 1 ? 1 : 0;
                $usuarioTarjeta->save();
            } else {
                $tarjetaRegistrada = 1;
            }
        }

        return Redirect::away('https://www.mibo.cl/pagos/2022?TBK_TOKEN=' . $request->TBK_TOKEN);

        //return view('resultado', compact('inscripcion', 'tarjetaRegistrada'));
    }

    public function buscarTarjeta(Request $request)
    {
        $token = base64_decode($request->token);

        $inscripcion = InscripcionTarjeta::where('tbk_token', $token)->first();

        return response()->json(
            [
                'clase' => $inscripcion->estado == 0 ? 'success' : 'danger',
                'mensaje' => $inscripcion->estadoDetalle
            ]
        );
    }

    public function borrarTarjeta(Request $request)
    {
        $tarjeta = UsuarioTarjeta::where('id', base64_decode($request->tarjeta))->first();
        $usuario =  User::where('id', $tarjeta->user_id)->first();

        $resp = (new MallInscription)->delete($tarjeta->tbk_user, $usuario->nombre);


        if ($resp == true) {
            UsuarioTarjeta::where('id', $tarjeta->id)->delete();
        }

        return response()->json(
            [
                'clase' => $resp == true ? 'success' : 'error',
                'mensaje' => $resp == true ? 'Tarjeta borrada exitosamente.' : 'Ha ocurrido un problema al borrar la tarjeta.'
            ]
        );
    }

    public function cambiarTarjetaPorDefecto(Request $request)
    {
        $tarjeta = UsuarioTarjeta::where('id', base64_decode($request->tarjeta))->first();

        UsuarioTarjeta::where('user_id', $tarjeta->user_id)->update(['defecto' => 0]);
        $tarjeta->defecto = 1;
        $tarjeta->save();

        return response()->json([
            'clase' => 'success',
            'mensaje' => 'Tarjeta cambiada exitosamente.'
        ]);
    }

    public function usuarioRealizarPagos(Request $request)
    {
        $pagos = json_decode($request->pagos, true);
        $usuario = base64_decode($request->usuario);
        $usuarioTarjeta = User::where('id_mibo', $usuario)->first();

        $tarjeta = UsuarioTarjeta::where('user_id', $usuarioTarjeta->id)->where('defecto', 1)->first();

        $usuarioPagosComercioAsociado = UsuarioPago::where('user_id', $usuarioTarjeta->id)->count();

        $numero_orden = 'MIBOPAY-' . ($usuarioPagosComercioAsociado + 1);

        foreach ($pagos as $key => $item) {
            $pagos[$key]["commerce_code"] = 597046009456;
            $pagos[$key]["installments_number"] = 1;
        }

        $resp = (new MallTransaction)->authorize($usuarioTarjeta->nombre, $tarjeta->tbk_user, $numero_orden, $pagos);
        $detallesPago = $resp->getDetails();

        $fecha = Carbon::parse($resp->transactionDate);

        $pago = UsuarioPago::create([
            'orden_compra' => $resp->buyOrder,
            'tarjeta_utilizada' => $resp->cardNumber,
            'fecha_transaccion' => $fecha->format('Y-m-d H:i:s'),
            'user_id' => $usuarioTarjeta->id,
            'usuario_tarjeta_id' => $tarjeta->id
        ]);

        $planes = $request->planes;

        foreach ($detallesPago as $index => $item) {
            UsuarioPagoDetalle::create([
                'detalle' => $planes[$index],
                'monto' => $item->getAmount(),
                'estado_webpay' => $item->getStatus(),
                'codigo_autorizacion' => $item->getAuthorizationCode(),
                'tipo_tarjeta_codigo' => $item->getPaymentTypeCode(),
                'estado' => $item->getResponseCode(),
                'codigo_comercio' => $item->getCommerceCode(),
                'orden_compra_hijo' => $item->getBuyOrder(),
                'usuario_pago_id' => $pago->id
            ]);

            /* $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.mibo.cl/contratos/UpdateEstadoPagoOC',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array(
                    'token'=> "MIBO235321XCONNECT24342020",
                    'identificador'=> $item->getBuyOrder(),
                    'metodo_pago'=> 600,
                    'fecha_pago'=> $fecha->format('Y-m-d'),
                    'comprobante'=> "http://www.mibo.cl/comprobantes",
                ) ,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Cookie: ci_session=amsdjn5266k5ef4jqi78joql5jff958k'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl); */
        }

        return response()->json([
            'detalle' => $pago->detalle,
            'pago' => $pago
        ]);

        //return $pago->detalle;
    }


    public function transacciones()
    {
        return view('ktipay.transacciones');
    }

    public function pagos()
    {
        return view('ktipay.pagos');
    }

    public function pagosUsuario(Request $request)
    {
        $usuario = User::find($request->usuario);
        $tarjeta = UsuarioTarjeta::where('user_id', $usuario->id)->first();

        $usuarioPagosComercioAsociado = UsuarioPago::where('user_id', $usuario->id)->count();


        $numero_orden = 'MIBOPAY-' . ($usuarioPagosComercioAsociado + 1);

        $resp = (new MallTransaction)->authorize($usuario->nombre, $tarjeta->tbk_user, $numero_orden, $request->pagos);
        $detallesPago = $resp->getDetails();

        $fecha = Carbon::parse($resp->transactionDate);

        $pago = UsuarioPago::create([
            'orden_compra' => $resp->buyOrder,
            'tarjeta_utilizada' => $resp->cardNumber,
            'fecha_transaccion' => $fecha->format('Y-m-d H:i:s'),
            'user_id' => $usuario->id,
            'usuario_tarjeta_id' => $tarjeta->id
        ]);

        foreach ($detallesPago as $item) {
            UsuarioPagoDetalle::create([
                'detalle' => 'Detalle',
                'monto' => $item->getAmount(),
                'estado_webpay' => $item->getStatus(),
                'codigo_autorizacion' => $item->getAuthorizationCode(),
                'tipo_tarjeta_codigo' => $item->getPaymentTypeCode(),
                'estado' => $item->getResponseCode(),
                'codigo_comercio' => $item->getCommerceCode(),
                'orden_compra_hijo' => $item->getBuyOrder(),
                'usuario_pago_id' => $pago->id
            ]);
        }

        return $pago->detalle;
    }

    public function transaccionesListado()
    {
        return Datatables::of(UsuarioPagoDetalle::with('pago')->orderBy('created_at', 'DESC')->get())->make(true);
    }

    public function pagoAutomatico($usuario, $monto)
    {

        $usuario = User::find($usuario);
        $tarjeta = UsuarioTarjeta::where('user_id', $usuario->id)->first();

        $usuarioPagos = UsuarioPago::count();
        $usuarioPagosComercioAsociado = UsuarioPago::where('user_id', $usuario->id)->count();

        $codigoComercio = '597046009447';

        $numero_orden = 'MIBOPAY-' . ($usuarioPagosComercioAsociado + 1);
        $numero_orden_hijo = 'KTIPAIH-' . ($usuarioPagos + 1);

        $detalles = [
            [
                "commerce_code" => $codigoComercio,
                "buy_order" => $numero_orden_hijo,
                "amount" => $monto,
                "installments_number" => 1
            ]
        ];


        $resp = (new MallTransaction)->authorize($usuario->nombre, $tarjeta->tbk_user, $numero_orden, $detalles);
        $detallesPago = $resp->getDetails();

        $fecha = Carbon::parse($resp->transactionDate);

        $pago = UsuarioPago::create([
            'orden_compra' => $resp->buyOrder,
            'tarjeta_utilizada' => $resp->cardNumber,
            'fecha_transaccion' => $fecha->format('Y-m-d H:i:s'),
            'user_id' => $usuario->id,
            'usuario_tarjeta_id' => $tarjeta->id
        ]);

        foreach ($detallesPago as $item) {
            UsuarioPagoDetalle::create([
                'detalle' => 'Detalle',
                'monto' => $item->getAmount(),
                'estado_webpay' => $item->getStatus(),
                'codigo_autorizacion' => $item->getAuthorizationCode(),
                'tipo_tarjeta_codigo' => $item->getPaymentTypeCode(),
                'estado' => $item->getResponseCode(),
                'codigo_comercio' => $item->getCommerceCode(),
                'orden_compra_hijo' => $item->getBuyOrder(),
                'usuario_pago_id' => $pago->id
            ]);
        }
    }

    public function pagoEstado($numero_orden)
    {
        $resp = (new MallTransaction)->status($numero_orden);

        $usuarioPago = UsuarioPago::where('orden_compra', $numero_orden)->first();

        if ($resp->details[0]->responseCode == 0) {
            $usuarioPago->codigo_autorizacion = $resp->details[0]->authorizationCode;
            $usuarioPago->tipo_tarjeta_codigo = $resp->details[0]->paymentTypeCode;
            $usuarioPago->estado = 1;
        } else {
            $usuarioPago->estado = 2;
        }

        $usuarioPago->save();

        return response()->json([
            'estado' => $usuarioPago->estado
        ]);
    }

    public function postPagoAutomatico(Request $request)
    {
        $response = [
            'estado' => true,
            'errores' => [],
            'errores_ordenes' => []
        ];

        if (!isset($request->usuario) || !is_numeric(base64_decode($request->usuario))) {
            array_push($response['errores'], 'Usuario no ingresado o no es númerico');
        }

        if (!isset($request->numero_orden) || empty($request->numero_orden)) {
            array_push($response['errores'], 'Número de orden no ingresado');
        }

        /*         if (!isset($request->monto) || !is_numeric(base64_decode($request->monto))) {
            array_push($response['errores'], 'Monto no ingresado no es númerico');
        } */

        if (count($response['errores']) > 0) {
            $response['estado'] = false;

            return $response;
        }

        $usuario = User::find(base64_decode($request->usuario));

        if (!$usuario) {
            array_push($response['errores'], 'Usuario no existente');
        }

        $tarjeta = UsuarioTarjeta::where('user_id', $usuario->id)->where('defecto', 1)->first();

        if (!$usuario) {
            array_push($response['errores'], 'Tarjeta de usuario no existente');
        }

        if (count($response['errores']) > 0) {
            $response['estado'] = false;

            return $response;
        }

        $ordenes_validas = [];

        foreach ($request->detalles as $item) {
            if (!isset($item['buy_order'])) {
                array_push($response['errores'], 'La información enviada no tiene un formato de código JSON correcto');
                $response['estado'] = false;
                return $response;
            }

            $valido = 1;

            if (!isset($item['amount']) || !is_numeric($item['amount']) || $item['amount'] < 0) {
                $valido = 0;
            }

            if (!isset($item['commerce_code'])) {
                $valido = 0;
            }

            if (!isset($item['buy_order'])) {
                $valido = 0;
            }

            if (!isset($item['installments_number']) || !is_numeric($item['installments_number']) || $item['installments_number'] < 1) {
                $valido = 0;
            }

            if ($valido == 0) {
                array_push($response['errores_ordenes'], $item['buy_order']);
                continue;
            } else {
                array_push($ordenes_validas, $item);
            }
        }

        if (count($ordenes_validas) == 0) {
            array_push($response['errores'], 'Debe existir al menos un detalle para proceder con webpay');
            $response['estado'] = false;

            return $response;
        }

        //$codigoComercio = '597046009447';
        $numero_orden = base64_decode($request->numero_orden);


        $resp = (new MallTransaction)->authorize($usuario->nombre, $tarjeta->tbk_user, $numero_orden, $ordenes_validas);
        $detallesPago = $resp->getDetails();

        $fecha = Carbon::parse($resp->transactionDate);

        $pago = UsuarioPago::create([
            'orden_compra' => $resp->buyOrder,
            'tarjeta_utilizada' => $resp->cardNumber,
            'fecha_transaccion' => $fecha->format('Y-m-d H:i:s'),
            'user_id' => $usuario->id,
            'usuario_tarjeta_id' => $tarjeta->id
        ]);

        foreach ($detallesPago as $index => $item) {
            UsuarioPagoDetalle::create([
                'detalle' => 'Detalle',
                'monto' => $item->getAmount(),
                'estado_webpay' => $item->getStatus(),
                'codigo_autorizacion' => $item->getAuthorizationCode(),
                'tipo_tarjeta_codigo' => $item->getPaymentTypeCode(),
                'estado' => $item->getResponseCode(),
                'codigo_comercio' => $item->getCommerceCode(),
                'orden_compra_hijo' => $item->getBuyOrder(),
                'usuario_pago_id' => $pago->id
            ]);
        }

        return response()->json($pago->makeHidden(['usuario', 'usuario_nombre', 'usuario_rut']));
    }

    public function postPagoEstado(Request $request)
    {
        $response = [
            'estado' => true,
            'errores' => []
        ];

        if (!isset($request->numero_orden)) {
            array_push($response['errores'], 'Número de orden no ingresado o no es númerico');
        }

        if (count($response['errores']) > 0) {
            $response['estado'] = false;

            return $response;
        }

        //$resp = (new MallTransaction)->status(base64_decode($request->numero_orden));

        //$usuarioPago = ;
        /*
        $usuarioPago->codigo_autorizacion = $resp->details[0]->authorizationCode;
        $usuarioPago->tipo_tarjeta_codigo = $resp->details[0]->paymentTypeCode;
        $usuarioPago->estado = $resp->details[0]->responseCode;

        $usuarioPago->save(); */

        //$response['pago_usuario'] = $usuarioPago;

        //return response()->json(UsuarioPagoDetalle::where('orden_compra', base64_decode($request->numero_orden))->first());

        return response()->json(UsuarioPago::where('orden_compra', base64_decode($request->numero_orden))->first());
    }

    public function postEliminarTarjeta(Request $request)
    {
        $response = [
            'estado' => true,
            'errores' => [],
        ];

        if (!isset($request->tarjeta)) {
            array_push($response['errores'], 'ID Tarjeta no ingresado');
        }

        if (count($response['errores']) > 0) {
            $response['estado'] = false;

            return $response;
        }

        $tarjeta = UsuarioTarjeta::where('tbk_user', base64_decode($request->tarjeta))->first();

        if (!$tarjeta) {
            array_push($response['errores'], 'Tarjeta no encontrada');
            $response['estado'] = false;

            return $response;
        }

        $usuario =  User::where('id', $tarjeta->user_id)->first();

        if (!$usuario) {
            array_push($response['errores'], 'Usuario no encontrada');
            $response['estado'] = false;

            return $response;
        }

        $resp = (new MallInscription)->delete($tarjeta->tbk_user, $usuario->nombre);


        if ($resp == true) {
            UsuarioTarjeta::where('id', $tarjeta->id)->delete();
        } else {
            array_push($response['errores'], 'Ha ocurrido un error al ejecutar al borrar la tarjeta');
            $response['estado'] = false;
        }

        return $response;
    }

    public function eliminarTarjeta(Request $request)
    {
        $tarjeta = UsuarioTarjeta::where('id', $request->id)->first();
        $usuario =  User::where('id', $tarjeta->user_id)->first();

        $resp = (new MallInscription)->delete($tarjeta->tbk_user, $usuario->nombre);

        $estado = 1;
        if ($resp == true) {
            UsuarioTarjeta::where('tbk_user', $tarjeta->tbk_user)->delete();
        } else {
            $estado = 0;
        }

        return response()->json([
            'estado' => $estado
        ]);
    }

    public function cambiarTarjetaDefecto(Request $request)
    {
        $tarjeta = UsuarioTarjeta::where('id', $request->id)->first();

        UsuarioTarjeta::where('user_id', $tarjeta->user_id)->update(['defecto' => 0]);
        $tarjeta->defecto = 1;
        $tarjeta->save();

        return response()->json([
            'estado' => 1
        ]);
    }
}
