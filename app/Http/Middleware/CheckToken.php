<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Token::where('token', $request->header('token'))->first();

        if (!$request->header('token') || !$token || $token->expirado) {
            return response()->json('Sin permisos', 401);
        }

        return $next($request);
    }
}
