<?php

namespace App\Http\Middleware;

use App\Acceso;
use App\Ingreso;
use App\Token;
use Carbon\Carbon;
use Closure;

class VerificarAccesos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ahora = Carbon::now()->format('Y-m-d H:i:s');
        $segundosAntes = Carbon::now()->subSecond(10)->format('Y-m-d H:i:s');
        $ingreso = Ingreso::where('ip_ingreso', $request->getClientIp())->first();

        if($ingreso && $ingreso->estado == 2){
            return response()->json('Sin permisos', 401);
        }

        if(!$ingreso){
            Ingreso::create([
                'ip_ingreso' => $request->getClientIp(),
                'creacion' => $ahora,
                'ultimo_acceso' => $ahora
            ]);

            return $next($request);
        }


        /* --------------------------------------------------------------------------- */

        $ingreso = Ingreso::firstOrCreate(['ip_ingreso' => $request->getClientIp()]);

        Acceso::create([
            'creacion' => $ahora,
            'ingreso_id' => $ingreso->id
        ]);

        $accesos = Acceso::where('ingreso_id', $ingreso->id)->whereBetween('created_at', [$segundosAntes ,$ahora])->count();
        $ultimoAcceso = Acceso::where('ingreso_id', $ingreso->id)->orderBy('id', 'desc')->first();

        if($accesos > 10){
            $ingreso->cantidad_accesos += 1;
            $ingreso->save();
        }

        if($ingreso->cantidad_accesos > 2){
            $ingreso->estado = 2;
            $ingreso->save();

            return response()->json('Sin permisos', 401);
        }

        if(Carbon::createFromFormat('Y-m-d H:i:s',  $ingreso->ultimo_acceso)->diffInSeconds(Carbon::now()) < 2){
            $ingreso->ultimo_acceso = $ahora;
            $ingreso->save();
            return response()->json('Forbidden', 403);
        }

        $ingreso->ultimo_acceso = $ahora;
        $ingreso->save();


        return $next($request);
    }
}
