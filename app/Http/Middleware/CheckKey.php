<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;

class CheckKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Token::where('token', $request->header('key'))->first();

        dd($token);

        if(!$token || !$token->expirado){
            return $next($request);
        }

        return response()->json('Sin permisos', 401);

    }
}
