<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    //
    protected $guarded = ['id'];
    protected $appends = ['expirado'];

    public function getExpiradoAttribute(){
        $termino = Carbon::createFromFormat('Y-m-d H:i:s',  $this->expiracion);

        return Carbon::now()->gt($termino);
    }
}
