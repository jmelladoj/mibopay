<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KTI PAY - Inscripción</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous" defer>
    </script>

    <script src="{{ secure_asset('js/app.js') }}" defer></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mx-auto">
                <div class="card mt-5 px-3 py-3">
                    <h5 class="card-title text-center">Inscripción</h5>
                    <div class="card-body">
                        <div class="row mb-1">
                            <label for="nombre" class="col-sm-4 col-form-label">Nombre :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nombre" value="Juan Mellado">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="email" class="col-sm-4 col-form-label">Correo :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="email"
                                    value="j.melladojimenez@hotmail.com">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="rut" class="col-sm-4 col-form-label">Rut :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="rut" value="18.685.728-3">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="rutRepresentante" class="col-sm-4 col-form-label">Rut representante :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="rutRepresentante" value="18.685.728-3">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="descripcion" class="col-sm-4 col-form-label">Descripción tarjeta :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="descripcion" value="Tarjeta de empresa">
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col">
                                <center>
                                    <input class="form-check-input" type="checkbox" value="" id="terminos">
                                    <label class="form-check-label" for="terminos">
                                        Acepto términos y condiciones
                                    </label>
                                    <i class="fas fa-info-circle text-info"
                                        onclick="abrirModalTerminosCondiciones()"></i>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="d-grid gap-2">
                                    <button class="btn btn-info text-white"
                                        onclick="validarUsuario()">Inscribirme</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modalTerminosCondiciones" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Términos y condiciones</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                onclick="cerrarModalTerminosCondiciones()" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="text-justify">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                                piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard
                                McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of
                                the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through
                                the cites of the word in classical literature, discovered the undoubtable source. Lorem
                                Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
                                Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the
                                theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum,
                                "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                                onclick="cerrarModalTerminosCondiciones()">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<script>
    $(document).ready(function() {
        console.log("ready!");
    });

    function abrirModalTerminosCondiciones() {
        $('#modalTerminosCondiciones').show();
    }

    function cerrarModalTerminosCondiciones() {
        $('#modalTerminosCondiciones').hide();
    }

    function incribirNuevaTarjeta() {
        window.location.replace("/");
    }

    async function validarUsuario() {
        try {
            const res = await axios.post(
                `/api/usuario/validar`, {
                    nombre: btoa($('#nombre').val()),
                    email: btoa($('#email').val()),
                    rut: btoa($('#rut').val()),
                    rutRepresentante: btoa($('#rutRepresentante').val()),
                    descripcion: $('#descripcion').val(),
                }
            );

            if (res.data) {
                let form = document.createElement("form");
                let input_token = document.createElement("input");

                form.method = "POST";
                form.action = res.data.url_webpay;

                input_token.value = res.data.tbk_token;
                input_token.name = "TBK_TOKEN";
                input_token.type = "hidden";
                form.appendChild(input_token);
                document.body.appendChild(form);
                form.submit();

            }

        } catch (error) {
            console.log(error);
        }
    }
</script>
