<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KTI PAY - Transacciones</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <style>
        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-danger {
            color: #fff;
            background-color: #dc3545;
        }

    </style>
</head>

<body>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12 mx-auto">
                <div class="card mt-5 px-3 py-3">
                    <h5 class="card-title text-center">Transacciones</h5>
                    <div class="card-body">
                        <div class="row mb-1">
                            <table id="tablaPagos"
                                class="table table-bordered table-sm table-stripped table-outlined table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-left">Cliente</th>
                                        <th class="text-left">Rut</th>
                                        <th class="text-left">Comercio</th>
                                        <th class="text-left">Id compra general</th>
                                        <th class="text-left">Id compra</th>
                                        <th class="text-left">Monto</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Fecha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</body>

</html>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
</script>

<script src="{{ asset('js/app.js') }}"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        tablaTransacciones.init()
    });


    async function validarUsuario() {
        try {
            const res = await axios.post(
                `/api/usuario/validar`, {
                    nombre: $('#nombre').val(),
                    email: $('#email').val(),
                    rut: $('#rut').val(),
                    rutRepresentante: $('#rutRepresentante').val()
                }
            );

            if (res.data) {
                let form = document.createElement("form");
                let input_token = document.createElement("input");

                form.method = "POST";
                form.action = res.data.url_webpay;

                input_token.value = res.data.tbk_token;
                input_token.name = "TBK_TOKEN";
                input_token.type = "hidden";
                form.appendChild(input_token);
                document.body.appendChild(form);
                form.submit();

            }

        } catch (error) {
            console.log(error);
        }
    }

    const tablaTransacciones = function() {
        var inicializarTabla = function() {

            // begin first table
            $('#tablaPagos').DataTable({
                responsive: true,
                destroy: true,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": " _START_ - _END_ de _TOTAL_ ",
                    "infoEmpty": "0 -  0 de 0 ",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": " _MENU_ ",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },

                ajax: {
                    url: '/api/usuario/listado/transacciones',
                    type: 'GET',
                    data: {
                        pagination: {
                            perpage: 50,
                        },
                    },
                },
                sDom: "<'row'><'row'<'col-md-1'l><'col-md-7'B><'col-md-4'f>r>t<'row'<'col-md-11'><'col-md-1'i>><'row'<'col-md-9'><'col-md-3'p>>",

                buttons: [{
                        extend: 'pdf',
                        text: 'PDF',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'excel',
                        text: 'EXCEL',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'csv',
                        text: 'CSV',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'print',
                        text: 'IMPRIMIR',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    }
                ],
                columns: [{
                        data: 'id',
                        className: 'text-center'
                    },
                    {
                        data: 'pago.usuario_nombre'
                    },
                    {
                        data: 'pago.usuario_rut'
                    },
                    {
                        data: 'codigo_comercio'
                    },
                    {
                        data: 'pago.orden_compra',
                    },
                    {
                        data: 'orden_compra_hijo',
                    },
                    {
                        data: 'monto',
                        render: function(data, type, full, meta) {
                            return new Intl.NumberFormat('es-CL', {
                                currency: 'CLP',
                                style: 'currency'
                            }).format(data);
                        }
                    },
                    {
                        className: 'text-center',
                        render: function(data, type, full, meta) {
                            let clase = full.estado == 0 ? 'badge-success' : 'badge-danger';
                            let mensaje = full.estado == 0 ? 'Aprobado' : 'No aprobado';

                            return `<span class="badge ${clase}" data-toggle="tooltip" data-placement="top" title="${full.estado_detalle}">${mensaje}</span>`;
                        }

                    },
                    {
                        data: 'pago.fecha',
                        className: 'text-center'
                    },
                ],

            });
        };

        return {
            init: function() {
                inicializarTabla();
            },

        };

    }();
</script>
