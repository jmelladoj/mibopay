<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KTI PAY - Inscripción</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous" defer>
    </script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mx-auto">
                <div class="card mt-5 px-3 py-3">
                    <h5 class="card-title text-center">Inscripción</h5>
                    <div class="card-body">
                        @isset($tarjetaRegistrada)
                            @if ($tarjetaRegistrada == 1)
                                <div class="alert alert-danger">
                                    La tarjeta ya se encuentra asociada al usuario
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-info text-white"
                                                onclick="incribirNuevaTarjeta()">Inscribir
                                                nueva tarjeta</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endisset
                        @isset($inscripcion->estado)
                            @if ($tarjetaRegistrada == 0)
                                <div class="alert {{ $inscripcion->estado == 0 ? 'alert-success' : 'alert-danger' }}">
                                    {{ $inscripcion->estadoDetalle }}
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-info text-white"
                                                onclick="incribirNuevaTarjeta()">Inscribir
                                                nueva tarjeta</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endisset
                    </div>
                </div>
            </div>

        </div>
    </div>
</body>

</html>

<script>
    $(document).ready(function() {
        //console.log("ready!");
    });


    function incribirNuevaTarjeta() {
        window.location.replace("/");
    }
</script>
